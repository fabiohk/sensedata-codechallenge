#!/bin/bash

[ "${INIT_DB}" == "true" ] && flask init-db --seed-swapi
waitress-serve --call "${FLASK_APP}:create_app"