FROM python:3.8-slim AS builder

# Copy all files from root to /app
WORKDIR /app
COPY . /app

# Install the project dependencies
RUN pip install --upgrade pip && \
	pip install pipenv && \
	pipenv install --system --deploy --ignore-pipfile

RUN useradd -m app-user
USER app-user

FROM builder AS dev
USER root
RUN pipenv install --system --dev --deploy --ignore-pipfile
USER app-user