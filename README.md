# SenseData Code Challenge

Project made with `Flask` and `Docker` :heart:!

* hosted preview: [https://sensedata-challenge.herokuapp.com/](https://sensedata-challenge.herokuapp.com/)
* username: `preview-user`
* password: `preview-pass`

## Features

* Show a table of characters from the Star Wars franchise that can be filtered by planet, starship, vehicles and/or movies. It can also sort the table by name, gender, 
weight or height
* Show a table of starships from the Star Wars franchie rated by `hyperdrive_rating / cost_in_credits`. The starships are shown from the best to lower 
rating.
* Uses a Basic Authentication to access the above contents.

## Running this project locally

You can either start the project with:

```bash
docker-compose up app
```

or:

```bash
docker-compose up app-dev
```

The first command will start a production environment and will be accesible by port 8080. The second command will start a development environment, 
acessible by port 5000.

### Initializing the database

In order to initialize the database, or to clear the existing tables, a new custom command was made! Just run:

```bash
flask init-db
```

If want to initialize the database with content from [SWAPI](http://swapi.dev/), run:

```bash
flask init-db --seed-swapi
```

### Creating a new user

To create a new user for this app usage:

```bash
flask new-user --username "username" --password "password"
```

The created `username` and `password` will be prompted when needed.

### Disclaimer

Remember, the command `flask init-db` is highly destructive! As stated, it clears the database tables every time that it runs, so if you want to preserve 
the current content, removed the `INIT_DB: "true"` from the `docker-compose.yaml` service (it appears under `environment` section) before running the 
desired `docker` command.


## Running tests

```bash
docker-compose up run-tests
```

The above command will run a `tox` environment that are previously configured to run the project tests.
