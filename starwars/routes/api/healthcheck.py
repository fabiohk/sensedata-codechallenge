"""Healthcheck routes."""

import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

logger = logging.getLogger(__name__)

blueprint = Blueprint("healthcheck_routes", __name__)


@blueprint.route("/")
def verify_health() -> flask.Response:
    """
    Verify if any app service is down.

    Currently, no dependency service is checked.
    """
    return make_response(jsonify({}), 200)
