"""Species routes."""
import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

from starwars.business import auth
from starwars.services.db.specie import get_specie
from starwars.services.db.specie import get_species

logger = logging.getLogger(__name__)

blueprint = Blueprint("species_routes", __name__)


@blueprint.route("/")
@auth.login_required
def list_species() -> flask.Response:
    """Retrieve species."""
    logger.info("Retrieving a list of species.")
    species = get_species()
    return make_response(jsonify({"items": species}), 200)


@blueprint.route("/<int:specie_id>/")
@auth.login_required
def retrieve_specie(specie_id: int) -> flask.Response:
    """Retrieve a specie given its id."""
    logger.info("Retrieving specie: %s", specie_id)
    specie = get_specie(specie_id)
    return make_response(jsonify(specie), 200)
