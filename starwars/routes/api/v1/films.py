"""Films routes."""
import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

from starwars.business import auth
from starwars.services.db.film import get_film
from starwars.services.db.film import get_films

logger = logging.getLogger(__name__)

blueprint = Blueprint("films_routes", __name__)


@blueprint.route("/")
@auth.login_required
def list_films() -> flask.Response:
    """Retrieve films."""
    logger.info("Retrieving a list of films.")
    films = get_films()
    return make_response(jsonify({"items": films}), 200)


@blueprint.route("/<int:film_id>/")
@auth.login_required
def retrieve_film(film_id: int) -> flask.Response:
    """Retrieve a film given its id."""
    logger.info("Retrieving film: %s", film_id)
    film = get_film(film_id)
    return make_response(jsonify(film), 200)
