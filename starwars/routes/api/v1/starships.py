"""Starships routes."""
import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

from starwars.business import auth
from starwars.business import retrieve_starships_rated
from starwars.services.db.starship import get_starship

logger = logging.getLogger(__name__)

blueprint = Blueprint("starships_routes", __name__)


@blueprint.route("/")
@auth.login_required
def list_starships() -> flask.Response:
    """
    Retrieve starships.

    Sorted by their rating and on descending order (from max to min).
    """
    starships = retrieve_starships_rated()
    return make_response(jsonify({"items": starships}), 200)


@blueprint.route("/<int:starship_id>/")
@auth.login_required
def retrieve_starship(starship_id: int) -> flask.Response:
    """Retrieve a starship given its id."""
    logger.info("Retrieving starship: %s", starship_id)
    starship = get_starship(starship_id)
    return make_response(jsonify(starship), 200)
