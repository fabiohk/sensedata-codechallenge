"""Person routes."""
import logging

from typing import Optional

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response
from webargs.flaskparser import parser

from starwars.business import auth
from starwars.business import retrieve_page_from_persons
from starwars.schemas.person_routes import RetrievePersonsSchema
from starwars.services.db import DBFilter

logger = logging.getLogger(__name__)

blueprint = Blueprint("persons_routes", __name__)


@blueprint.route("/")
@parser.use_kwargs(RetrievePersonsSchema, location="query")
@auth.login_required
def list_persons(
    page: int,
    sort_by: str,
    sort_order: str,
    film: Optional[int] = None,
    vehicle: Optional[int] = None,
    planet: Optional[int] = None,
    starship: Optional[int] = None,
) -> flask.Response:
    """
    Retrieve all stored persons.

    It uses the following query params:
        'page': The page that should be returned (starts at 1);
        'sort_by': The field that should be used for sorting (one of 'name', 'gender', 'weight' or 'height'), defaults
        to 'name';
        'sort_order': In which order the sort should be made (one of 'ASC', 'DESC'), defaults to 'ASC';
        'film': The film id that should be used as filter;
        'vehicle': The vehicle id that should be used as filter;
        'planet': The planet id that should be used as filter.
    """
    logger.info(
        "Retrieving persons with the following params: page - %s, sort_by - %s, sort_order - %s, film - %s, "
        "vehicle - %s, planet - %s, starship - %s",
        page,
        sort_by,
        sort_order,
        film,
        vehicle,
        planet,
        starship,
    )

    filters = [
        filter_option
        for filter_option in [
            DBFilter(table="person", field="homeworld", value=planet),
            DBFilter(table="film_character", field="film_id", value=film),
            DBFilter(table="vehicle_pilot", field="vehicle_id", value=vehicle),
            DBFilter(table="starship_pilot", field="starship_id", value=starship),
        ]
        if filter_option.value
    ]

    result = retrieve_page_from_persons(page, sort_by, sort_order=sort_order, filters=filters)

    return make_response(jsonify(result), 200)
