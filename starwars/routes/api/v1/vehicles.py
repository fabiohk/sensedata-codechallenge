"""Vehicles routes."""
import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

from starwars.business import auth
from starwars.services.db.vehicle import get_vehicle
from starwars.services.db.vehicle import get_vehicles

logger = logging.getLogger(__name__)

blueprint = Blueprint("vehicles_routes", __name__)


@blueprint.route("/")
@auth.login_required
def list_vehicles() -> flask.Response:
    """Retrieve vehicles."""
    logger.info("Retrieving a list of vehicles.")
    vehicles = get_vehicles()
    return make_response(jsonify({"items": vehicles}), 200)


@blueprint.route("/<int:vehicle_id>/")
@auth.login_required
def retrieve_vehicle(vehicle_id: int) -> flask.Response:
    """Retrieve a vehicle given its id."""
    logger.info("Retrieving vehicle: %s", vehicle_id)
    vehicle = get_vehicle(vehicle_id)
    return make_response(jsonify(vehicle), 200)
