"""Planets routes."""
import logging

import flask

from flask import Blueprint
from flask import jsonify
from flask import make_response

from starwars.business import auth
from starwars.services.db.planet import get_planet
from starwars.services.db.planet import get_planets

logger = logging.getLogger(__name__)

blueprint = Blueprint("planets_routes", __name__)


@blueprint.route("/")
@auth.login_required
def list_planets() -> flask.Response:
    """Retrieve planets."""
    logger.info("Retrieving a list of planets.")
    planets = get_planets()
    return make_response(jsonify({"items": planets}), 200)


@blueprint.route("/<int:planet_id>/")
@auth.login_required
def retrieve_planet(planet_id: int) -> flask.Response:
    """Retrieve a planet given its id."""
    logger.info("Retrieving planet: %s", planet_id)
    planet = get_planet(planet_id)
    return make_response(jsonify(planet), 200)
