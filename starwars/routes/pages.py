"""Pages routes."""
import logging

from typing import Optional

import flask

from flask import Blueprint
from flask import make_response
from flask import render_template
from marshmallow import EXCLUDE
from webargs.flaskparser import parser

from starwars.business import auth
from starwars.business import retrieve_page_from_persons
from starwars.business import retrieve_starships_rated
from starwars.schemas.person_routes import RetrievePersonsSchema
from starwars.services.db import DBFilter
from starwars.services.db.film import get_film
from starwars.services.db.film import get_films
from starwars.services.db.planet import get_planet
from starwars.services.db.planet import get_planets
from starwars.services.db.specie import get_specie
from starwars.services.db.starship import get_starship
from starwars.services.db.vehicle import get_vehicle
from starwars.services.db.vehicle import get_vehicles
from starwars.support.pagination_utils import get_max_pages_from_sequence_length

logger = logging.getLogger(__name__)

blueprint = Blueprint("pages_routes", __name__)


@blueprint.route("/")
def index() -> flask.Response:
    """Retrieve the index page."""
    return make_response(render_template("index.html"), 200)


@blueprint.route("/healthcheck/")
def healthcheck() -> flask.Response:
    """Retrieve the healthcheck page."""
    return make_response(render_template("healthcheck.html"), 200)


@blueprint.route("/persons/", methods=("GET", "POST"))
@parser.use_kwargs(RetrievePersonsSchema(unknown=EXCLUDE), location="form")
@auth.login_required
def persons(
    page: int,
    sort_by: str,
    sort_order: str,
    film: Optional[int] = None,
    vehicle: Optional[int] = None,
    planet: Optional[int] = None,
    starship: Optional[int] = None,
) -> flask.Response:
    """Retrieve the persons page."""
    logger.info(
        "Loading persons page with the following params: page - %s, sort_by - %s, sort_order - %s, film - %s, "
        "vehicle - %s, planet - %s, starship - %s",
        page,
        sort_by,
        sort_order,
        film,
        vehicle,
        planet,
        starship,
    )

    filters = [
        filter_option
        for filter_option in [
            DBFilter(table="person", field="homeworld", value=planet),
            DBFilter(table="film_character", field="film_id", value=film),
            DBFilter(table="vehicle_pilot", field="vehicle_id", value=vehicle),
            DBFilter(table="starship_pilot", field="starship_id", value=starship),
        ]
        if filter_option.value
    ]

    result = retrieve_page_from_persons(page, sort_by, sort_order=sort_order, filters=filters)
    persons = []
    for person in result["items"]:
        person["homeworld"] = (
            get_planet(planet_id=person["homeworld"])["name"] if person["homeworld"] else person["homeworld"]
        )
        person["films"] = ", ".join(get_film(film)["title"] for film in person["films"]) if person["films"] else ""
        person["species"] = (
            ", ".join(get_specie(specie)["name"] for specie in person["species"]) if person["species"] else ""
        )
        person["starships"] = (
            ", ".join(get_starship(starship)["name"] for starship in person["starships"]) if person["starships"] else ""
        )
        person["vehicles"] = (
            ", ".join(get_vehicle(vehicle)["name"] for vehicle in person["vehicles"]) if person["vehicles"] else ""
        )
        persons.append(person)

    starships, films, planets, vehicles = retrieve_starships_rated(), get_films(), get_planets(), get_vehicles()

    return make_response(
        render_template(
            "persons.html",
            persons=persons,
            starships=starships,
            films=films,
            planets=planets,
            vehicles=vehicles,
            max_pages=get_max_pages_from_sequence_length(result["total"]),
            current_page=page,
            sort_by=sort_by,
            selected_planet=planet,
            selected_film=film,
            selected_vehicle=vehicle,
            selected_starship=starship,
            sort_order=sort_order,
        ),
        200,
    )


@blueprint.route("/starships/")
@auth.login_required
def starships() -> flask.Response:
    """Retrieve the starships page."""
    logger.info("Loading starships page")
    starships = retrieve_starships_rated()
    return make_response(render_template("starships.html", starships=starships), 200)
