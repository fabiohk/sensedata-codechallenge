"""Module that gives a interface for queries within the person table."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Person
from starwars.services.db import create_instance
from starwars.services.db import execute_select_query
from starwars.services.db import retrieve_from_table


def create_person(person: Person, should_commit: bool = True) -> Person:
    """Insert a new person to database."""
    query = """
        INSERT INTO
            person (name, birth_year, eye_color, gender, hair_color, height, mass, skin_color, homeworld, updated_at)
        VALUES
            (:name, :birth_year, :eye_color, :gender, :hair_color, :height, :mass, :skin_color, :homeworld, :updated_at)
        """
    create_instance(query, should_commit, **person)
    return person


def get_persons(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Person]:
    """Retrieve persons from database."""
    query_result = retrieve_from_table(
        "person", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Person(row) for row in query_result]


def get_filtered_persons(
    sort_field: str = "person.created_at",
    sort_order: str = "DESC",
    filters: Optional[List[DBFilter]] = None,
    limit: Optional[int] = None,
) -> List[Person]:
    """
    Retrieve the persons from database, given the options from 'filters'.

    The table content can be limited to at most 'limit' rows and can be sorted by 'sort_field' in a 'sort_order'.
    """
    query = """
        SELECT
            person.id AS id,
            person.created_at AS created_at,
            person.updated_at AS updated_at,
            person.name AS name,
            person.birth_year AS birth_year,
            person.eye_color AS eye_color,
            person.gender AS gender,
            person.hair_color AS hair_color,
            COALESCE(person.height, 'unknown') AS height,
            COALESCE(person.mass, 'unknown') AS mass,
            person.skin_color AS skin_color,
            person.homeworld AS homeworld,
            film_character.film_id AS film_id,
            person_specie.specie_id AS specie_id,
            vehicle_pilot.vehicle_id AS vehicle_id,
            vehicle_pilot.vehicle_id AS starship_id
        FROM
            person
        LEFT JOIN
            film_character ON film_character.character_id = person.id
        LEFT JOIN
            person_specie ON person_specie.person_id = person.id
        LEFT JOIN
            vehicle_pilot ON vehicle_pilot.pilot_id = person.id
        LEFT JOIN
            starship_pilot ON starship_pilot.pilot_id = person.id
        """
    query_result = execute_select_query(
        query, sort_order=sort_order, sort_field=sort_field, filters=filters, limit=limit
    )

    persons_dict = {}
    for row in query_result:
        row_dict = dict(row)
        film_id, specie_id, vehicle_id, starship_id = (
            row_dict.pop("film_id"),
            row_dict.pop("specie_id"),
            row_dict.pop("vehicle_id"),
            row_dict.pop("starship_id"),
        )
        film = [film_id] if film_id else []
        specie = [specie_id] if specie_id else []
        vehicle = [vehicle_id] if vehicle_id else []
        starship = [starship_id] if starship_id else []

        if row_dict["id"] not in persons_dict:
            persons_dict[row_dict["id"]] = {
                **row_dict,
                "films": set(film),
                "species": set(specie),
                "vehicles": set(vehicle),
                "starships": set(starship),
            }
        else:
            persons_dict[row_dict["id"]]["films"].update(film)
            persons_dict[row_dict["id"]]["species"].update(specie)
            persons_dict[row_dict["id"]]["vehicles"].update(vehicle)
            persons_dict[row_dict["id"]]["starships"].update(starship)

    return list(persons_dict.values())
