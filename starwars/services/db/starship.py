"""Module that gives a interface for queries within the starship database."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Starship
from starwars.services.db import create_instance
from starwars.services.db import execute_select_query
from starwars.services.db import retrieve_from_table


def create_starship(starship: Starship, should_commit: bool = True) -> Starship:
    """Insert a new starship to database."""
    query = """
        INSERT INTO
            starship (name, model, class, manufacturer, cost_in_credits, length, crew, passengers, max_atmosphere_speed,
            hyperdrive_rating, MGLT, cargo_capacity, consumables, updated_at)
        VALUES
            (:name, :model, :starship_class, :manufacturer, :cost_in_credits, :length, :crew, :passengers,
            :max_atmosphere_speed, :hyperdrive_rating, :MGLT, :cargo_capacity, :consumables, :updated_at)
        """
    create_instance(query, should_commit, **starship)
    return starship


def get_starships(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Starship]:
    """Retrieve starships from database."""
    query_result = retrieve_from_table(
        "starship", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Starship(row) for row in query_result]


def get_starships_rated(
    rating_statement: str, sort_field: str = "id", sort_order: str = "DESC", limit: Optional[int] = None
) -> List[Starship]:
    """Retrieve starships with their rating, given by the 'rating_statement'."""
    query = f"""
        SELECT
            starship.id AS id,
            starship.created_at AS created_at,
            starship.updated_at AS updated_at,
            starship.name AS name,
            starship.model AS model,
            starship.class AS class,
            starship.manufacturer AS manufacturer,
            COALESCE(starship.cost_in_credits, 'unknown') AS cost_in_credits,
            starship.length AS length,
            starship.crew AS crew,
            starship.passengers AS passengers,
            starship.max_atmosphere_speed AS max_atmosphere_speed,
            COALESCE(starship.hyperdrive_rating, 'unknown') AS hyperdrive_rating,
            starship.MGLT AS MGLT,
            starship.cargo_capacity AS cargo_capacity,
            starship.consumables AS consumables,
            COALESCE({rating_statement}, 0) AS rating
        FROM
            starship
        """

    query_result = execute_select_query(query, sort_field=sort_field, sort_order=sort_order, limit=limit)
    return [Starship(row) for row in query_result]


def get_starship(starship_id: int) -> Starship:
    """Retrieve a starship from database."""
    query_result: List[Starship] = retrieve_from_table(
        "starship", filters=[DBFilter(table="starship", field="id", value=starship_id)], sort_field="id", sort_order=""
    )
    return Starship(query_result[0])
