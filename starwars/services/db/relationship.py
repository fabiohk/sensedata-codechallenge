"""Module that gives a interface for queries within the relationship tables."""

from starwars.services.db import FilmCharacter
from starwars.services.db import FilmPlanet
from starwars.services.db import FilmSpecie
from starwars.services.db import FilmStarship
from starwars.services.db import FilmVehicle
from starwars.services.db import PersonSpecie
from starwars.services.db import StarshipPilot
from starwars.services.db import VehiclePilot
from starwars.services.db import create_instance


def create_film_character(film_character: FilmCharacter, should_commit: bool = True) -> FilmCharacter:
    """Ïnsert a new film character to database."""
    query = """
        INSERT INTO
            film_character (film_id, character_id, updated_at)
        VALUES
            (:film_id, :character_id, :updated_at)
        """
    create_instance(query, should_commit, **film_character)
    return film_character


def create_person_specie(person_specie: PersonSpecie, should_commit: bool = True) -> PersonSpecie:
    """Ïnsert a new person specie to database."""
    query = """
        INSERT INTO
            person_specie (person_id, specie_id, updated_at)
        VALUES
            (:person_id, :specie_id, :updated_at)
        """
    create_instance(query, should_commit, **person_specie)
    return person_specie


def create_vehicle_pilot(vehicle_pilot: VehiclePilot, should_commit: bool = True) -> VehiclePilot:
    """Ïnsert a new vehicle pilot to database."""
    query = """
        INSERT INTO
            vehicle_pilot (vehicle_id, pilot_id, updated_at)
        VALUES
            (:vehicle_id, :pilot_id, :updated_at)
        """
    create_instance(query, should_commit, **vehicle_pilot)
    return vehicle_pilot


def create_starship_pilot(starship_pilot: StarshipPilot, should_commit: bool = True) -> StarshipPilot:
    """Ïnsert a new starship pilot to database."""
    query = """
        INSERT INTO
            starship_pilot (starship_id, pilot_id, updated_at)
        VALUES
            (:starship_id, :pilot_id, :updated_at)
        """
    create_instance(query, should_commit, **starship_pilot)
    return starship_pilot


def create_film_specie(film_specie: FilmSpecie, should_commit: bool = True) -> FilmSpecie:
    """Insert a new film specie to database."""
    query = """
        INSERT INTO
            film_specie (film_id, specie_id, updated_at)
        VALUES
            (:film_id, :specie_id, :updated_at)
        """
    create_instance(query, should_commit, **film_specie)
    return film_specie


def create_film_vehicle(film_vehicle: FilmVehicle, should_commit: bool = True) -> FilmVehicle:
    """Ïnsert a new film vehicle to database."""
    query = """
        INSERT INTO
            film_vehicle (film_id, vehicle_id, updated_at)
        VALUES
            (:film_id, :vehicle_id, :updated_at)
        """
    create_instance(query, should_commit, **film_vehicle)
    return film_vehicle


def create_film_starship(film_starship: FilmStarship, should_commit: bool = True) -> FilmStarship:
    """Ïnsert a new film starship to database."""
    query = """
        INSERT INTO
            film_starship (film_id, starship_id, updated_at)
        VALUES
            (:film_id, :starship_id, :updated_at)
        """
    create_instance(query, should_commit, **film_starship)
    return film_starship


def create_film_planet(film_planet: FilmPlanet, should_commit: bool = True) -> FilmPlanet:
    """Insert a new film planet to database."""
    query = """
        INSERT INTO
            film_planet (film_id, planet_id, updated_at)
        VALUES
            (:film_id, :planet_id, :updated_at)
        """
    create_instance(query, should_commit, **film_planet)
    return film_planet
