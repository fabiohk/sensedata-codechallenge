"""Module that gives a interface for connection with the database."""

import logging
import sqlite3

from collections import namedtuple
from datetime import date
from datetime import datetime
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Sequence
from typing import Set
from typing import Tuple
from typing import TypedDict

from flask import current_app
from flask import g

logger = logging.getLogger(__name__)

Person = TypedDict(
    "Person",
    {
        "name": str,
        "birth_year": str,
        "eye_color": str,
        "gender": str,
        "hair_color": str,
        "height": float,
        "mass": float,
        "skin_color": str,
        "homeworld": int,
        "updated_at": datetime,
        "created_at": datetime,
        "films": Set[int],  # Set of movies (id) that the person appeared
        "species": Set[int],  # Set of species (id) that the person belongs to
        "vehicles": Set[int],  # Set os vehicles (id) that the person piloted
        "starships": Set[int],  # Set os starships (id) that the person piloted
        "id": int,
    },
    total=False,
)

Film = TypedDict(
    "Film",
    {
        "title": str,
        "episode_id": int,
        "opening_crawl": str,
        "director": str,
        "producer": str,
        "release_date": date,
        "updated_at": datetime,
        "created_at": datetime,
        "id": int,
    },
    total=False,
)
Vehicle = TypedDict(
    "Vehicle",
    {
        "name": str,
        "model": str,
        "vehicle_class": str,
        "manufacturer": str,
        "cost_in_credits": float,
        "length": str,
        "crew": str,
        "passengers": str,
        "max_atmosphere_speed": str,
        "cargo_capacity": str,
        "consumables": str,
        "updated_at": datetime,
        "created_at": datetime,
        "id": int,
    },
    total=False,
)
Starship = TypedDict(
    "Starship",
    {
        "name": str,
        "model": str,
        "starship_class": str,
        "manufacturer": str,
        "cost_in_credits": float,
        "length": str,
        "crew": str,
        "passengers": str,
        "max_atmosphere_speed": str,
        "cargo_capacity": str,
        "consumables": str,
        "hyperdrive_rating": float,
        "MGLT": str,
        "rating": float,
        "updated_at": datetime,
        "created_at": datetime,
        "id": int,
    },
    total=False,
)
Specie = TypedDict(
    "Specie",
    {
        "name": str,
        "classification": str,
        "designation": str,
        "average_height": str,
        "average_lifespan": str,
        "eye_colors": str,
        "hair_colors": str,
        "skin_colors": str,
        "language": str,
        "homeworld": int,
        "updated_at": datetime,
        "created_at": datetime,
        "id": int,
    },
    total=False,
)
Planet = TypedDict(
    "Planet",
    {
        "name": str,
        "diameter": str,
        "rotation_period": str,
        "orbital_period": str,
        "gravity": str,
        "population": str,
        "climate": str,
        "terrain": str,
        "surface_water": str,
        "updated_at": datetime,
        "created_at": datetime,
        "id": int,
    },
    total=False,
)
User = TypedDict(
    "User", {"username": str, "password": str, "updated_at": datetime, "created_at": datetime, "id": int}, total=False
)

# Many-to-Many tables

FilmCharacter = TypedDict(
    "FilmCharacter",
    {"film_id": int, "character_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
PersonSpecie = TypedDict(
    "PersonSpecie",
    {"person_id": int, "specie_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
VehiclePilot = TypedDict(
    "VehiclePilot",
    {"vehicle_id": int, "pilot_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
StarshipPilot = TypedDict(
    "StarshipPilot",
    {"starship_id": int, "pilot_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
FilmSpecie = TypedDict(
    "FilmSpecie",
    {"film_id": int, "specie_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
FilmVehicle = TypedDict(
    "FilmVehicle",
    {"film_id": int, "vehicle_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
FilmStarship = TypedDict(
    "FilmStarship",
    {"film_id": int, "starship_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)
FilmPlanet = TypedDict(
    "FilmPlanet",
    {"film_id": int, "planet_id": int, "updated_at": datetime, "created_at": datetime, "id": int},
    total=False,
)


DBFilter = namedtuple("DBFilter", ["table", "field", "value"])


def get_db() -> sqlite3.Connection:
    """Retrieve the database connector."""
    if not hasattr(g, "db"):
        g.db = sqlite3.connect(current_app.config["DATABASE"], detect_types=sqlite3.PARSE_DECLTYPES)
        g.db.row_factory = sqlite3.Row
    return g.db


def close_db(exception: Optional[Exception] = None):
    """Close the database connection."""
    if exception:
        logger.warning("Unhandled exception: %s", exception)

    if hasattr(g, "db"):
        g.db.close()


def init_db():
    """Ïnitialize the database."""
    db = get_db()

    with current_app.open_resource("schema.sql") as f:
        db.executescript(f.read().decode("utf8"))


def create_instance(query: str, should_commit: bool = True, **kwargs):
    """
    Create a new instance.

    The 'query' parameter **must** be an 'INSERT INTO' query.
    """
    kwargs["updated_at"] = datetime.now()
    execute_query(query, should_commit=should_commit, **kwargs)


def retrieve_from_table(
    table: str,
    sort_field: str,
    sort_order: str,
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List:
    """
    Retrieve data from a table.

    The table content can be filtered by 'filters', limited to at most 'limit' rows and can be sorted by 'sort_field' in
    a 'sort_order'.
    """
    query = f"SELECT * FROM {table}"
    return execute_select_query(query, sort_order=sort_order, sort_field=sort_field, limit=limit, filters=filters)


def execute_select_query(
    query: str,
    sort_order: str = "",
    filters: Optional[Sequence[DBFilter]] = None,
    sort_field: Optional[str] = None,
    group_field: Optional[str] = None,
    limit: Optional[int] = None,
    **kwargs,
) -> List:
    """Run a 'SELECT' query."""
    filter_query, filter_kwargs = _build_filter(filters)
    group_query, group_kwargs = _build_group(group_field)
    limit_query, limit_kwargs = _build_limit(limit)
    sort_query = _build_sort(sort_order, sort_field)

    query += filter_query + group_query + sort_query + limit_query
    return execute_query(query, should_commit=False, **kwargs, **filter_kwargs, **group_kwargs, **limit_kwargs)


def execute_query(query: str, should_commit: bool, **kwargs) -> List:
    """Execute a query and return the result row."""
    logger.debug("Query: %s, Kwargs: %s", query, kwargs)

    db = get_db()
    cur = db.execute(query, kwargs)
    rv = cur.fetchall()
    cur.close()

    if should_commit:
        db.commit()

    return rv


def count_table(table: str) -> int:
    """Count the number of rows of a table."""
    query = f"""
        SELECT
            COUNT(id) AS total
        FROM
            {table}
        """

    query_result = execute_query(query, should_commit=False)
    return query_result[0]["total"]


def get_filter_query(filter_option: DBFilter) -> str:
    """Retrieve a filter query from filter option."""
    table, field, value = filter_option.table, filter_option.field, filter_option.value
    return f"{table}.{field} = :{field}" if value is not None else f"{table}.{field} IS NULL"


def _build_filter_query(filters: Sequence[DBFilter]) -> str:
    """
    Build the 'WHERE' part from a list of filters.

    Returns an empty string if no filter is given as argument.
    """
    if not filters:
        return ""

    query = " WHERE " + " AND ".join(get_filter_query(filter_option) for filter_option in filters)
    return query


def _build_filter_kwargs(filters: Sequence[DBFilter]) -> Dict:
    """
    Build the kwargs with the named argument and value.

    Returns an empty dictionary if no filter is given as argument.
    """
    return {filter_option.field: filter_option.value for filter_option in filters}


def _build_filter(filters: Optional[Sequence[DBFilter]] = None) -> Tuple[str, Dict]:
    filter_options = filters if filters else []
    return _build_filter_query(filter_options), _build_filter_kwargs(filter_options)


def _build_sort(sort_order: str, sort_field: Optional[str] = None) -> str:
    return f" ORDER BY {sort_field} {sort_order}" if sort_field else ""


def _build_group(group_field: Optional[str] = None) -> Tuple[str, Dict]:
    return (" GROUP BY :group_field", {"group_field": group_field}) if group_field else ("", {})


def _build_limit(limit: Optional[int] = None) -> Tuple[str, Dict]:
    return (" LIMIT :limit", {"limit": limit}) if limit else ("", {})
