"""Module that gives a interface for queries within the specie table."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Specie
from starwars.services.db import create_instance
from starwars.services.db import retrieve_from_table


def create_specie(specie: Specie, should_commit: bool = True) -> Specie:
    """Ïnsert a new specie to database."""
    query = """
        INSERT INTO
            specie (name, classification, designation, average_height, average_lifespan, eye_colors, hair_colors,
            skin_colors, language, homeworld, updated_at)
        VALUES
            (:name, :classification, :designation, :average_height, :average_lifespan, :eye_colors, :hair_colors,
            :skin_colors, :language, :homeworld, :updated_at)
        """
    create_instance(query, should_commit, **specie)
    return specie


def get_species(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Specie]:
    """Retrieve species from database."""
    query_result = retrieve_from_table(
        "specie", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Specie(row) for row in query_result]


def get_specie(specie_id: int) -> Specie:
    """Retrieve a specie from database."""
    query_result = retrieve_from_table(
        "specie", filters=[DBFilter(table="specie", field="id", value=specie_id)], sort_field="id", sort_order=""
    )
    return Specie(query_result[0])
