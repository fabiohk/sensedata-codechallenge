"""Module that gives a interface for queries within the film table."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Film
from starwars.services.db import create_instance
from starwars.services.db import retrieve_from_table


def create_film(film: Film, should_commit: bool = True) -> Film:
    """Ïnsert a new film to database."""
    query = """
        INSERT INTO
            film (title, episode_id, opening_crawl, director, producer, release_date, updated_at)
        VALUES
            (:title, :episode_id, :opening_crawl, :director, :producer, :release_date, :updated_at)
        """
    create_instance(query, should_commit, **film)
    return film


def get_films(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Film]:
    """Retrieve films from database."""
    query_result = retrieve_from_table(
        "film", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Film(row) for row in query_result]


def get_film(film_id: int) -> Film:
    """Retrieve a film from database."""
    query_result: List[Film] = retrieve_from_table(
        "film", filters=[DBFilter(table="film", field="id", value=film_id)], sort_field="id", sort_order=""
    )
    return Film(query_result[0])
