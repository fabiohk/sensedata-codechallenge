"""Module that gives a interface for queries within the user table."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import User
from starwars.services.db import create_instance
from starwars.services.db import retrieve_from_table


def create_user(user: User, should_commit: bool = True) -> User:
    """Ïnsert a new user to database."""
    query = """
        INSERT INTO
            user (username, password, updated_at)
        VALUES
            (:username, :password, :updated_at)
        """
    create_instance(query, should_commit, **user)
    return user


def get_users(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[User]:
    """Retrieve users from database."""
    query_result = retrieve_from_table(
        "user", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [User(row) for row in query_result]


def get_user(user_id: int) -> User:
    """Retrieve a user from database."""
    query_result = retrieve_from_table(
        "user", filters=[DBFilter(table="user", field="id", value=user_id)], sort_field="id", sort_order=""
    )
    return User(query_result[0])
