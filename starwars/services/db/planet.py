"""Module that gives a interface for queries within the planet table."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Planet
from starwars.services.db import create_instance
from starwars.services.db import retrieve_from_table


def create_planet(planet: Planet, should_commit: bool = True) -> Planet:
    """Ïnsert a new planet to database."""
    query = """
        INSERT INTO
            planet (name, diameter, rotation_period, orbital_period, gravity, population, climate, terrain,
            surface_water, updated_at)
        VALUES
            (:name, :diameter, :rotation_period, :orbital_period, :gravity, :population, :climate, :terrain,
            :surface_water, :updated_at)
        """
    create_instance(query, should_commit, **planet)
    return planet


def get_planets(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Planet]:
    """Retrieve planets from database."""
    query_result = retrieve_from_table(
        "planet", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Planet(row) for row in query_result]


def get_planet(planet_id: int) -> Planet:
    """Retrieve a planet from database."""
    query_result: List[Planet] = retrieve_from_table(
        "planet", filters=[DBFilter(table="planet", field="id", value=planet_id)], sort_field="id", sort_order=""
    )
    return Planet(query_result[0])
