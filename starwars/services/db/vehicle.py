"""Module that gives a interface for queries within the vehicle database."""

from typing import List
from typing import Optional
from typing import Sequence

from starwars.services.db import DBFilter
from starwars.services.db import Vehicle
from starwars.services.db import create_instance
from starwars.services.db import retrieve_from_table


def create_vehicle(vehicle: Vehicle, should_commit: bool = True) -> Vehicle:
    """Insert a new vehichle to database."""
    query = """
        INSERT INTO
            vehicle (name, model, class, manufacturer, cost_in_credits, length, crew, passengers, max_atmosphere_speed,
            cargo_capacity, consumables, updated_at)
        VALUES
            (:name, :model, :vehicle_class, :manufacturer, :cost_in_credits, :length, :crew, :passengers,
            :max_atmosphere_speed, :cargo_capacity, :consumables, :updated_at)
        """
    create_instance(query, should_commit, **vehicle)
    return vehicle


def get_vehicles(
    sort_field: str = "id",
    sort_order: str = "DESC",
    limit: Optional[int] = None,
    filters: Optional[Sequence[DBFilter]] = None,
) -> List[Vehicle]:
    """Retrieve vehicles from database."""
    query_result = retrieve_from_table(
        "vehicle", sort_field=sort_field, sort_order=sort_order, limit=limit, filters=filters
    )
    return [Vehicle(row) for row in query_result]


def get_vehicle(vehicle_id: int) -> Vehicle:
    """Retrieve a vehicle from database."""
    query_result = retrieve_from_table(
        "vehicle", filters=[DBFilter(table="vehicle", field="id", value=vehicle_id)], sort_field="id", sort_order=""
    )
    return Vehicle(query_result[0])
