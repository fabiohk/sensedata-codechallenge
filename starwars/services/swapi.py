"""Module that gives an interface for connection with the SWAPI (https://swapi.dev)."""

import logging
import os

from functools import lru_cache
from typing import Any
from typing import Dict
from typing import Generator
from typing import Optional

from requests import Request

from starwars.support.http_utils import TimeoutConfig
from starwars.support.http_utils import get_response_body
from starwars.support.http_utils import request

logger = logging.getLogger(__name__)


class SWAPIClient:
    """Class that handle requests to the SWAPI (https://swapi.dev)."""

    BASE_URL = os.getenv("SWAPI_HOST", "https://swapi.dev/api")
    DEFAULT_TIMEOUT = float(os.getenv("DEFAULT_TIMEOUT", "5"))

    def _send_request(self, method: str, url: str, timeout: Optional[TimeoutConfig] = None) -> Any:
        req = Request(method, url)
        with request() as r:
            prepped = r.prepare_request(req)
            response = r.send(prepped, timeout=timeout if timeout else self.DEFAULT_TIMEOUT)
        return get_response_body(response)

    def _get_all_resource(self, resource: str) -> Generator[Dict, None, None]:
        has_next, url = True, f"{self.BASE_URL}/{resource}/"
        while has_next:
            response = self._send_request("GET", url)
            has_next, url = response["next"] is not None, response["next"]
            yield from response["results"]

    def get_all_persons(self) -> Generator[Dict, None, None]:
        """
        Retrieve all persons from SWAPI.

        Uses the resource: /people/
        """
        yield from self._get_all_resource("people")

    def get_all_films(self) -> Generator[Dict, None, None]:
        """
        Retrieve all films from SWAPI.

        Uses the resource: /films/
        """
        yield from self._get_all_resource("films")

    def get_all_starships(self) -> Generator[Dict, None, None]:
        """
        Retrieve all persons from SWAPI.

        Uses the resource: /starships/
        """
        yield from self._get_all_resource("starships")

    def get_all_vehicles(self) -> Generator[Dict, None, None]:
        """
        Retrieve all persons from SWAPI.

        Uses the resource: /vehicles/
        """
        yield from self._get_all_resource("vehicles")

    def get_all_species(self) -> Generator[Dict, None, None]:
        """
        Retrieve all persons from SWAPI.

        Uses the resource: /species/
        """
        yield from self._get_all_resource("species")

    def get_all_planets(self) -> Generator[Dict, None, None]:
        """
        Retrieve all persons from SWAPI.

        Uses the resource: /planets/
        """
        yield from self._get_all_resource("planets")

    @lru_cache
    def get_planet(self, planet_id: int) -> Dict:
        """
        Retrieve a planet from SWAPI.

        Uses the resource: /planets/:planet_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/planets/{planet_id}")

    @lru_cache
    def get_film(self, film_id: int) -> Dict:
        """
        Retrieve a film from SWAPI.

        Uses the resource: /films/:film_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/films/{film_id}")

    @lru_cache
    def get_vehicle(self, vehicle_id: int) -> Dict:
        """
        Retrieve a vehicle from SWAPI.

        Uses the resource: /vehicles/:vehicle_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/vehicles/{vehicle_id}")

    @lru_cache
    def get_starship(self, starship_id: int) -> Dict:
        """
        Retrieve a starship from SWAPI.

        Uses the resource: /starships/:starship_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/starships/{starship_id}")

    @lru_cache
    def get_person(self, people_id: int) -> Dict:
        """
        Retrieve a people from SWAPI.

        Uses the resource: /people/:people_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/people/{people_id}")

    @lru_cache
    def get_specie(self, specie_id: int) -> Dict:
        """
        Retrieve a specie from SWAPI.

        Uses the resource: /species/:specie_id.
        """
        return self._send_request("GET", f"{self.BASE_URL}/species/{specie_id}")


swapi_client = SWAPIClient()
