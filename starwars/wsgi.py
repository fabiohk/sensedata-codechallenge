"""Module with a factory that creates the app."""

from flask import Flask

from starwars.config.setup import register_custom_commands
from starwars.config.setup import register_routes
from starwars.config.setup import set_appcontext_teardown
from starwars.config.setup import set_project_config


def create_app():
    """Create the Star Wars application which is a Flask app."""
    app = Flask(__name__, instance_relative_config=True)

    set_project_config(app)
    set_appcontext_teardown(app)
    register_routes(app)
    register_custom_commands(app)

    return app
