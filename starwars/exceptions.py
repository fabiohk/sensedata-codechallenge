"""Module with custom exceptions."""

from werkzeug.exceptions import BadRequest


class UserAlreadyExists(BadRequest):
    description = "User with given username already exists."
