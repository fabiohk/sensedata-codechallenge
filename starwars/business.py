"""Module with method that deals with the business rules."""
import logging

from typing import Callable
from typing import List
from typing import Optional
from typing import Sequence

from marshmallow import EXCLUDE
from marshmallow import Schema
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

from flask_httpauth import HTTPBasicAuth
from starwars.exceptions import UserAlreadyExists
from starwars.schemas.swapi import FilmCharacterSchema
from starwars.schemas.swapi import FilmPlanetSchema
from starwars.schemas.swapi import FilmSchema
from starwars.schemas.swapi import FilmSpecieSchema
from starwars.schemas.swapi import FilmStarshipSchema
from starwars.schemas.swapi import FilmVehicleSchema
from starwars.schemas.swapi import PersonSchema
from starwars.schemas.swapi import PersonSpecieSchema
from starwars.schemas.swapi import PlanetSchema
from starwars.schemas.swapi import SpecieSchema
from starwars.schemas.swapi import StarshipPilotSchema
from starwars.schemas.swapi import StarshipSchema
from starwars.schemas.swapi import VehiclePilotSchema
from starwars.schemas.swapi import VehicleSchema
from starwars.services.db import DBFilter
from starwars.services.db import Starship
from starwars.services.db import User
from starwars.services.db.film import create_film
from starwars.services.db.film import get_films
from starwars.services.db.person import create_person
from starwars.services.db.person import get_filtered_persons
from starwars.services.db.person import get_persons
from starwars.services.db.planet import create_planet
from starwars.services.db.planet import get_planets
from starwars.services.db.relationship import create_film_character
from starwars.services.db.relationship import create_film_planet
from starwars.services.db.relationship import create_film_specie
from starwars.services.db.relationship import create_film_starship
from starwars.services.db.relationship import create_film_vehicle
from starwars.services.db.relationship import create_person_specie
from starwars.services.db.relationship import create_starship_pilot
from starwars.services.db.relationship import create_vehicle_pilot
from starwars.services.db.specie import create_specie
from starwars.services.db.specie import get_species
from starwars.services.db.starship import create_starship
from starwars.services.db.starship import get_starships
from starwars.services.db.starship import get_starships_rated
from starwars.services.db.user import create_user
from starwars.services.db.user import get_users
from starwars.services.db.vehicle import create_vehicle
from starwars.services.db.vehicle import get_vehicles
from starwars.services.swapi import swapi_client
from starwars.support.pagination_utils import PaginatedSequence
from starwars.support.pagination_utils import get_page_from_sequence

logger = logging.getLogger(__name__)

auth = HTTPBasicAuth()


def retrieve_starships_rated() -> List[Starship]:
    """
    Retrieve all starships with their rating.

    The rating is given by: starship.hyperdrive_rating / starship.cost_in_credits.
    """
    starships = get_starships_rated(
        rating_statement="starship.hyperdrive_rating / starship.cost_in_credits", sort_field="rating", sort_order="DESC"
    )
    return starships


def retrieve_page_from_persons(
    page: int, sort_by: str, sort_order: str = "ASC", page_size: int = 10, filters: Optional[List[DBFilter]] = None
) -> PaginatedSequence:
    """
    Retrieve a page (a slice) of at most 'page_size' persons.

    The sequence is sorted by 'sort_by' in a 'sort_order'.
    """
    persons = get_filtered_persons(sort_field=f"person.{sort_by}", sort_order=sort_order, filters=filters)
    return get_page_from_sequence(persons, page_index=page, page_size=page_size)


def seed_db_with_swapi_content():
    """Seed the database with SWAPI content."""

    def _do_create(
        sequence: Sequence, schema: Schema, create_function: Callable, get_last_element: Optional[Callable] = None
    ) -> List:
        created_elements = []
        for element in sequence:
            new_element = schema.load(element)
            create_function(new_element)
            if get_last_element:
                last_created_element = get_last_element()
                created_elements.append(last_created_element)
        return created_elements

    logger.info("Creating films data...")
    films = list(swapi_client.get_all_films())
    _do_create(films, FilmSchema(unknown=EXCLUDE), create_film, lambda: get_films(1))
    logger.info("Done!")

    logger.info("Creating planets data...")
    planets = list(swapi_client.get_all_planets())
    _do_create(planets, PlanetSchema(unknown=EXCLUDE), create_planet, lambda: get_planets(1))
    logger.info("Done!")

    logger.info("Creating vehicles data...")
    vehicles = list(swapi_client.get_all_vehicles())
    _do_create(vehicles, VehicleSchema(unknown=EXCLUDE), create_vehicle, lambda: get_vehicles(1))
    logger.info("Done!")

    logger.info("Creating starships data...")
    starships = list(swapi_client.get_all_starships())
    _do_create(starships, StarshipSchema(unknown=EXCLUDE), create_starship, lambda: get_starships(1))
    logger.info("Done!")

    logger.info("Creating species data...")
    species = list(swapi_client.get_all_species())
    _do_create(species, SpecieSchema(unknown=EXCLUDE), create_specie, lambda: get_species(1))
    logger.info("Done!")

    logger.info("Creating persons data...")
    persons = list(swapi_client.get_all_persons())
    _do_create(persons, PersonSchema(unknown=EXCLUDE), create_person, lambda: get_persons(1))
    logger.info("Done!")

    logger.info("Creating link between films and planets...")
    for planet in planets:
        _do_create(
            [{"planet": planet["url"], "film": film} for film in planet["films"]],
            FilmPlanetSchema(unknown=EXCLUDE),
            create_film_planet,
        )
    logger.info("Done!")

    logger.info("Creating link between films and vehicles...")
    for vehicle in vehicles:
        _do_create(
            [{"vehicle": vehicle["url"], "film": film} for film in vehicle["films"]],
            FilmVehicleSchema(unknown=EXCLUDE),
            create_film_vehicle,
        )
    logger.info("Done!")

    logger.info("Creating link between films and starships...")
    for starship in starships:
        _do_create(
            [{"starship": starship["url"], "film": film} for film in starship["films"]],
            FilmStarshipSchema(unknown=EXCLUDE),
            create_film_starship,
        )
    logger.info("Done!")

    logger.info("Creating link between films and species...")
    for specie in species:
        _do_create(
            [{"specie": specie["url"], "film": film} for film in specie["films"]],
            FilmSpecieSchema(unknown=EXCLUDE),
            create_film_specie,
        )
    logger.info("Done!")

    for person in persons:
        logger.info("Creating link between person and films...")
        _do_create(
            [{"person": person["url"], "film": film} for film in person["films"]],
            FilmCharacterSchema(unknown=EXCLUDE),
            create_film_character,
        )
        logger.info("Done!")

        logger.info("Creating link between person and species...")
        _do_create(
            [{"person": person["url"], "specie": specie} for specie in person["species"]],
            PersonSpecieSchema(unknown=EXCLUDE),
            create_person_specie,
        )
        logger.info("Done!")

        logger.info("Creating link between person and vehicle...")
        _do_create(
            [{"person": person["url"], "vehicle": vehicle} for vehicle in person["vehicles"]],
            VehiclePilotSchema(unknown=EXCLUDE),
            create_vehicle_pilot,
        )
        logger.info("Done!")

        logger.info("Creating link between person and starship...")
        _do_create(
            [{"person": person["url"], "starship": starship} for starship in person["starships"]],
            StarshipPilotSchema(unknown=EXCLUDE),
            create_starship_pilot,
        )
        logger.info("Done!")

    logger.info("Succesfully imported data from SWAPI!")


def create_new_user(username: str, password: str):
    """Create a new user for app usage."""
    filters = [DBFilter(table="user", field="username", value=username)]
    query_result = get_users(filters=filters)

    if query_result:
        raise UserAlreadyExists

    create_user(User(username=username, password=generate_password_hash(password)))


@auth.verify_password
def is_valid_user_credentials(username: str, password: str) -> bool:
    """Verify if given user credentials are valid."""
    filters = [DBFilter(table="user", field="username", value=username)]
    query_result = get_users(filters=filters)

    if not query_result:
        return False

    hash_password = query_result[0]["password"]
    return check_password_hash(hash_password, password)
