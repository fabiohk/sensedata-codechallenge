DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS film;
DROP TABLE IF EXISTS vehicle;
DROP TABLE IF EXISTS specie;
DROP TABLE IF EXISTS planet;
DROP TABLE IF EXISTS starship;

DROP TABLE IF EXISTS film_character;
DROP TABLE IF EXISTS person_specie;
DROP TABLE IF EXISTS vehicle_pilot;
DROP TABLE IF EXISTS film_specie;
DROP TABLE IF EXISTS film_vehicle;
DROP TABLE IF EXISTS film_planet;
DROP TABLE IF EXISTS starship_pilot;
DROP TABLE IF EXISTS film_starship;

DROP TABLE IF EXISTS user;

CREATE TABLE person (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	name TEXT NOT NULL,
	birth_year TEXT NOT NULL,
	eye_color TEXT NOT NULL,
	gender TEXT NOT NULL,
	hair_color TEXT NOT NULL,
	height REAL NULL,
	mass REAL NULL,
	skin_color TEXT NOT NULL,
	homeworld INTEGER,
	FOREIGN KEY (homeworld) REFERENCES planet (id)
);

CREATE TABLE film (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	title TEXT NOT NULL,
	episode_id INTEGER NOT NULL,
	opening_crawl TEXT NOT NULL,
	director TEXT NOT NULL,
	producer TEXT NOT NULL,
	release_date DATE NOT NULL
);

CREATE TABLE starship (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	name TEXT NOT NULL,
	model TEXT NOT NULL,
	class TEXT NOT NULL,
	manufacturer TEXT NOT NULL,
	cost_in_credits REAL,
	length TEXT NOT NULL,
	crew TEXT NOT NULL,
	passengers TEXT NOT NULL,
	max_atmosphere_speed TEXT NOT NULL,
	hyperdrive_rating REAL,
	MGLT TEXT NOT NULL,
	cargo_capacity TEXT NOT NULL,
	consumables TEXT NOT NULL
);

CREATE TABLE vehicle (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	name TEXT NOT NULL,
	model TEXT NOT NULL,
	class TEXT NOT NULL,
	manufacturer TEXT NOT NULL,
	cost_in_credits REAL,
	length TEXT NOT NULL,
	crew TEXT NOT NULL,
	passengers TEXT NOT NULL,
	max_atmosphere_speed TEXT NOT NULL,
	cargo_capacity TEXT NOT NULL,
	consumables TEXT NOT NULL
);

CREATE TABLE specie (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	name TEXT NOT NULL,
	classification TEXT NOT NULL,
	designation TEXT NOT NULL,
	average_height TEXT NOT NULL,
	average_lifespan TEXT NOT NULL,
	eye_colors TEXT NOT NULL,
	hair_colors TEXT NOT NULL,
	skin_colors TEXT NOT NULL,
	language TEXT NOT NULL,
	homeworld INTEGER,
	FOREIGN KEY (homeworld) REFERENCES planet (id)
);

CREATE TABLE planet (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	name TEXT NOT NULL,
	diameter TEXT NOT NULL,
	rotation_period TEXT NOT NULL,
	orbital_period TEXT NOT NULL,
	gravity TEXT NOT NULL,
	population TEXT NOT NULL,
	climate TEXT NOT NULL,
	terrain TEXT NOT NULL,
	surface_water TEXT NOT NULL
);

-- Many-to-Many relationship tables

CREATE TABLE film_character (
	-- Table that holds the characters that appeared on a film.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	film_id INTEGER NOT NULL,
	character_id INTEGER NOT NULL,
	FOREIGN KEY (film_id) REFERENCES film (id),
	FOREIGN KEY (character_id) REFERENCES person (id)
);

CREATE TABLE person_specie (
	-- Table that holds the species that a person belongs to.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	person_id INTEGER NOT NULL,
	specie_id INTEGER NOT NULL,
	FOREIGN KEY (person_id) REFERENCES person (id),
	FOREIGN KEY (specie_id) REFERENCES specie (id)
);

CREATE TABLE starship_pilot (
	-- Table that holds the pilots that piloted a starship.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	starship_id INTEGER NOT NULL,
	pilot_id INTEGER NOT NULL,
	FOREIGN KEY (starship_id) REFERENCES starship (id),
	FOREIGN KEY (pilot_id) REFERENCES person (id)
);

CREATE TABLE vehicle_pilot (
	-- Table that holds the pilots that piloted a vehicle.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	vehicle_id INTEGER NOT NULL,
	pilot_id INTEGER NOT NULL,
	FOREIGN KEY (vehicle_id) REFERENCES vehicle (id),
	FOREIGN KEY (pilot_id) REFERENCES person (id)
);

CREATE TABLE film_specie (
	-- Table that holds the species that appeared on a film.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	film_id INTEGER NOT NULL,
	specie_id INTEGER NOT NULL,
	FOREIGN KEY (film_id) REFERENCES film (id),
	FOREIGN KEY (specie_id) REFERENCES specie (id)
);

CREATE TABLE film_starship (
	-- Table that holds the starships that appeared on a film.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	film_id INTEGER NOT NULL,
	starship_id INTEGER NOT NULL,
	FOREIGN KEY (film_id) REFERENCES film (id),
	FOREIGN KEY (starship_id) REFERENCES starship (id)
);

CREATE TABLE film_vehicle (
	-- Table that holds the vehicles that appeared on a film.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	film_id INTEGER NOT NULL,
	vehicle_id INTEGER NOT NULL,
	FOREIGN KEY (film_id) REFERENCES film (id),
	FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)
);

CREATE TABLE film_planet (
	-- Table that holds the planets that appeared on a film.
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	film_id INTEGER NOT NULL,
	planet_id INTEGER NOT NULL,
	FOREIGN KEY (film_id) REFERENCES film (id),
	FOREIGN KEY (planet_id) REFERENCES planet (id)
);

CREATE TABLE user (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
)