"""Module that contains a helper methods for pagination."""

import math
import os

from functools import lru_cache
from typing import Sequence
from typing import Tuple
from typing import TypedDict

DEFAULT_PAGINATION = int(os.getenv("DEFAULT_PAGINATION", "10"))

PaginatedSequence = TypedDict("PaginatedSequence", {"total": int, "items": Sequence})


def get_page_from_sequence(
    sequence: Sequence, page_index: int, page_size: int = DEFAULT_PAGINATION
) -> PaginatedSequence:
    """
    Retrieve the page 'page_index' of 'page_size' from a sequence.

    The page sequence starts at index 1.
    """
    if page_index < 1:
        raise ValueError("'page_index' must be greater than 0!")

    if page_size < 1:
        raise ValueError("'page_size' must be greater than 0")

    total = len(sequence)
    left_idx, right_idx = _get_page_indexes(page_index, page_size)
    return PaginatedSequence(total=total, items=sequence[left_idx:right_idx])


@lru_cache
def get_max_pages_from_sequence_length(sequence_length: int, page_size: int = DEFAULT_PAGINATION) -> int:
    """Retrieve the maximum number of pages of size 'page_size' from a sequence with length 'sequence_length'."""
    return math.ceil(sequence_length / page_size)


def _get_page_indexes(page_index: int, page_size: int) -> Tuple[int, int]:
    return (page_index - 1) * page_size, page_index * page_size
