"""Module with operating system helper functions."""

import os


def create_directories_if_needed(path: str):
    """Create all directories from the passed 'path'."""
    try:
        os.makedirs(path)
    except FileExistsError:
        pass
