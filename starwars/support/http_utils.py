"""Module with utility functions for HTTP requests."""
import logging

from contextlib import contextmanager
from typing import Any
from typing import Dict
from typing import Generator
from typing import Iterable
from typing import Optional
from typing import Tuple
from typing import Union

from requests import Response
from requests import Session
from requests import Timeout
from requests.adapters import HTTPAdapter
from urllib3 import Retry

logger = logging.getLogger(__name__)

TimeoutConfig = Union[Tuple[float, float], float]


@contextmanager
def request(
    total: int = 3,
    backoff_factor: float = 0.1,
    method_whitelist: Iterable[str] = Retry.DEFAULT_METHOD_WHITELIST,
    headers: Optional[Dict] = None,
    **kwargs: Any,
) -> Generator[Session, None, None]:
    """
    Generate a requests session that allows retries and raises HTTP errors (status code >= 400).

    Uses the same arguments as the class Retry from urllib3
    """
    session = Session()
    session.hooks.update(response=[_log_request_response, _check_for_errors])
    max_retries = Retry(total=total, backoff_factor=backoff_factor, method_whitelist=method_whitelist, **kwargs)
    adapter = HTTPAdapter(max_retries=max_retries)
    session.mount("https://", adapter)
    session.mount("http://", adapter)
    if headers:
        session.headers.update(headers)
    try:
        yield session
    except Timeout as err:
        logger.warning("Request timed-out: %s", err)
        raise
    finally:
        session.close()


def get_response_body(response: Response) -> Any:
    """Deserialize response."""
    try:
        response_body = response.json()
    except ValueError:
        response_body = response.text
    return response_body


def _check_for_errors(response: Response, *args: Any, **kwargs: Any):
    response.raise_for_status()


def _log_request_response(response: Response, *args: Any, **kwargs: Any):
    logger.debug(
        "Request to %s returned the status code: %s "
        "Request body: %s "
        "Request headers: %s "
        "Response body: %s "
        "Response headers: %s.",
        response.url,
        response.status_code,
        response.request.body,
        response.request.headers,
        response.text,
        response.headers,
    )
