"""Module with utility functions for regex."""
import re

from typing import Optional


def pick_first_integer_from_string(string: str) -> Optional[int]:
    """Pick the first integer that appears on a string. If no integer is found, returns None."""
    found_integers = re.findall(r"[0-9]+", string)
    return int(found_integers[0]) if found_integers else None
