"""Marshmallow schemas used on the person routes."""

from typing import Dict

from marshmallow import Schema
from marshmallow import fields
from marshmallow import pre_load
from marshmallow import validate


class RetrievePersonsSchema(Schema):
    """
    Marshmallow schema used to parse the parameters for 'retrieve_persons' route.

    'page': The page that should be returned (starts at 1);
    'sort_by': The field that should be used for sorting (one of 'name', 'gender', 'weight' or 'height'), defaults
    to 'name';
    'sort_order': In which order the sort should be made (one of 'ASC', 'DESC'), defaults to 'ASC'
    'film': The film id that should be used as filter;
    'vehicle': The vehicle id that should be used as filter;
    'starship': The starship id that should be used as filter;
    'planet': The planet id that should be used as filter.
    """

    page = fields.Integer(validate=validate.Range(min=1, min_inclusive=True), missing=1)
    sort_by = fields.String(validate=validate.OneOf(["name", "gender", "mass", "height"]), missing="name")
    film = fields.Integer(validate=validate.Range(min=1, min_inclusive=True))
    vehicle = fields.Integer(validate=validate.Range(min=1, min_inclusive=True))
    planet = fields.Integer(validate=validate.Range(min=1, min_inclusive=True))
    starship = fields.Integer(validate=validate.Range(min=1, min_inclusive=True))
    sort_order = fields.String(validate=validate.OneOf(["ASC", "DESC"]), missing="ASC")

    @pre_load
    def remove_empty_string_from_data(self, data: Dict, *args, **kwargs) -> Dict:
        return {key: data[key] for key in data if data[key] != ""}
