"""Marshmallow schemas used parse the SWAPI content for internal usage."""

import logging
import re

from typing import Dict
from typing import Optional

from marshmallow import EXCLUDE
from marshmallow import Schema
from marshmallow import fields
from marshmallow import pre_load

from starwars.services.db import DBFilter
from starwars.services.db.film import get_films
from starwars.services.db.person import get_persons
from starwars.services.db.planet import get_planets
from starwars.services.db.specie import get_species
from starwars.services.db.starship import get_starships
from starwars.services.db.vehicle import get_vehicles
from starwars.services.swapi import swapi_client
from starwars.support.regex_utils import pick_first_integer_from_string

logger = logging.getLogger(__name__)


class FilmSchema(Schema):
    title = fields.String()
    episode_id = fields.Integer()
    opening_crawl = fields.String()
    director = fields.String()
    producer = fields.String()
    release_date = fields.Date("%Y-%m-%d")


class PlanetSchema(Schema):
    name = fields.String()
    rotation_period = fields.String()
    orbital_period = fields.String()
    diameter = fields.String()
    climate = fields.String()
    gravity = fields.String()
    terrain = fields.String()
    surface_water = fields.String()
    population = fields.String()


class _BaseVehicleSchema(Schema):
    name = fields.String()
    model = fields.String()
    manufacturer = fields.String()
    cost_in_credits = fields.Float(allow_none=True)
    length = fields.String()
    crew = fields.String()
    passengers = fields.String()
    max_atmosphere_speed = fields.String(data_key="max_atmosphering_speed")
    cargo_capacity = fields.String()
    consumables = fields.String()

    @pre_load
    def translate_unknown_to_none_for_cost_in_credits(self, data: Dict, *args, **kwargs) -> Dict:
        if data["cost_in_credits"] == "unknown":
            data["cost_in_credits"] = None
        return data


class VehicleSchema(_BaseVehicleSchema):
    vehicle_class = fields.String()


class StarshipSchema(_BaseVehicleSchema):
    starship_class = fields.String()
    hyperdrive_rating = fields.Float(allow_none=True)
    MGLT = fields.String()

    @pre_load
    def translate_unknown_to_none_for_hyperdrive_rating(self, data: Dict, *args, **kwargs) -> Dict:
        if data["hyperdrive_rating"] == "unknown":
            data["hyperdrive_rating"] = None
        return data


class _PlanetSearchSchema(Schema):
    def get_planet(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for planet: %s", value)

        if not value:
            return None

        # breakpoint()
        planet_id = pick_first_integer_from_string(value)
        swapi_planet = PlanetSchema(unknown=EXCLUDE).load(swapi_client.get_planet(planet_id))

        filters = [DBFilter(table="planet", field=field, value=value) for field, value in swapi_planet.items()]
        planet = get_planets(filters=filters)[0]
        return planet["id"]


class SpecieSchema(_PlanetSearchSchema):
    name = fields.String()
    classification = fields.String()
    designation = fields.String()
    average_height = fields.String()
    average_lifespan = fields.String()
    eye_colors = fields.String()
    hair_colors = fields.String()
    skin_colors = fields.String()
    language = fields.String()
    homeworld = fields.Method(deserialize="get_planet", allow_none=True)


class PersonSchema(_PlanetSearchSchema):
    name = fields.String()
    birth_year = fields.String()
    eye_color = fields.String()
    gender = fields.String()
    hair_color = fields.String()
    height = fields.Float(allow_none=True)
    mass = fields.Float(allow_none=True)
    skin_color = fields.String()
    homeworld = fields.Method(deserialize="get_planet", allow_none=True)

    @pre_load
    def translate_unknown_to_none_for_mass(self, data: Dict, *args, **kwargs) -> Dict:
        if not data["mass"]:
            return data

        data["mass"] = data["mass"].replace(",", ".")
        if data["mass"] == "unknown":
            data["mass"] = None
        return data

    @pre_load
    def translate_unknown_to_none_for_height(self, data: Dict, *args, **kwargs) -> Dict:
        if not data["height"]:
            return data

        data["height"] = data["height"].replace(",", ".")
        if data["height"] == "unknown":
            data["height"] = None
        return data


class _FilmSearchSchema(Schema):
    def get_film(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for film: %s", value)

        if not value:
            return None

        film_id = pick_first_integer_from_string(value)
        swapi_film = FilmSchema(unknown=EXCLUDE).load(swapi_client.get_film(film_id))

        filters = [DBFilter(table="film", field=field, value=value) for field, value in swapi_film.items()]
        film = get_films(filters=filters)[0]
        return film["id"]


class FilmPlanetSchema(_PlanetSearchSchema, _FilmSearchSchema):
    film_id = fields.Method(deserialize="get_film", data_key="film")
    planet_id = fields.Method(deserialize="get_planet", data_key="planet")


class _VehicleSearchSchema(Schema):
    def get_vehicle(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for vehicle: %s", value)

        if not value:
            return None

        vehicle_id = pick_first_integer_from_string(value)
        swapi_vehicle = VehicleSchema(unknown=EXCLUDE).load(swapi_client.get_vehicle(vehicle_id))
        swapi_vehicle["class"] = swapi_vehicle.pop("vehicle_class")

        filters = [DBFilter(table="vehicle", field=field, value=value) for field, value in swapi_vehicle.items()]
        vehicle = get_vehicles(filters=filters)[0]
        return vehicle["id"]


class FilmVehicleSchema(_VehicleSearchSchema, _FilmSearchSchema):
    film_id = fields.Method(deserialize="get_film", data_key="film")
    vehicle_id = fields.Method(deserialize="get_vehicle", data_key="vehicle")


class _StarshipSearchSchema(Schema):
    def get_starship(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for starship: %s", value)
        if not value:
            return None

        starship_id = pick_first_integer_from_string(value)
        swapi_starship = StarshipSchema(unknown=EXCLUDE).load(swapi_client.get_starship(starship_id))
        swapi_starship["class"] = swapi_starship.pop("starship_class")

        filters = [DBFilter(table="starship", field=field, value=value) for field, value in swapi_starship.items()]
        starship = get_starships(filters=filters)[0]
        return starship["id"]


class FilmStarshipSchema(_StarshipSearchSchema, _FilmSearchSchema):
    film_id = fields.Method(deserialize="get_film", data_key="film")
    starship_id = fields.Method(deserialize="get_starship", data_key="starship")


class _HomeworldSearchSchema(Schema):
    homeworld = fields.Method(deserialize="get_homeworld", allow_none=True)

    def get_homeworld(self, value: Optional[str]) -> Optional[int]:
        logger.debug("Value received for homeworld: %s", value)

        if not value:
            return None

        return pick_first_integer_from_string(value)


class _SpecieSearchSchema(_HomeworldSearchSchema):
    def get_specie(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for specie: %s", value)

        if not value:
            return None

        specie_id = pick_first_integer_from_string(value)
        swapi_specie = swapi_client.get_specie(specie_id)

        parsed_swapi_specie = SpecieSchema(unknown=EXCLUDE, exclude=("homeworld",)).load(swapi_specie)
        parsed_swapi_specie["homeworld"] = self.get_homeworld(swapi_specie["homeworld"])

        filters = [DBFilter(table="specie", field=field, value=value) for field, value in parsed_swapi_specie.items()]
        specie = get_species(filters=filters)[0]
        return specie["id"]


class FilmSpecieSchema(_SpecieSearchSchema, _FilmSearchSchema):
    film_id = fields.Method(deserialize="get_film", data_key="film")
    specie_id = fields.Method(deserialize="get_specie", data_key="specie")


class _PersonSearchSchema(_HomeworldSearchSchema):
    def get_person(self, value: Optional[str] = None) -> Optional[int]:
        logger.debug("Value received for person: %s", value)

        if not value:
            return None

        person_id = pick_first_integer_from_string(value)
        swapi_person = swapi_client.get_person(person_id)

        parsed_swapi_person = PersonSchema(unknown=EXCLUDE, exclude=("homeworld",)).load(swapi_person)
        parsed_swapi_person["homeworld"] = self.get_homeworld(swapi_person["homeworld"])

        filters = [DBFilter(table="person", field=field, value=value) for field, value in parsed_swapi_person.items()]
        person = get_persons(filters=filters)[0]
        return person["id"]


class FilmCharacterSchema(_PersonSearchSchema, _FilmSearchSchema):
    film_id = fields.Method(deserialize="get_film", data_key="film")
    character_id = fields.Method(deserialize="get_person", data_key="person")


class PersonSpecieSchema(_PersonSearchSchema, _SpecieSearchSchema):
    person_id = fields.Method(deserialize="get_person", data_key="person")
    specie_id = fields.Method(deserialize="get_specie", data_key="specie")


class StarshipPilotSchema(_StarshipSearchSchema, _PersonSearchSchema):
    pilot_id = fields.Method(deserialize="get_person", data_key="person")
    starship_id = fields.Method(deserialize="get_starship", data_key="starship")


class VehiclePilotSchema(_VehicleSearchSchema, _PersonSearchSchema):
    pilot_id = fields.Method(deserialize="get_person", data_key="person")
    vehicle_id = fields.Method(deserialize="get_vehicle", data_key="vehicle")
