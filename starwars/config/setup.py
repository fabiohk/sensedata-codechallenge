"""Module with setup functions for the application."""

import logging
import logging.config
import os

from flask import Flask

from starwars.commands import init_db_command
from starwars.commands import new_user_command
from starwars.routes import pages
from starwars.routes.api import healthcheck
from starwars.routes.api.v1 import films as films_v1
from starwars.routes.api.v1 import person as person_v1
from starwars.routes.api.v1 import planets as planets_v1
from starwars.routes.api.v1 import species as species_v1
from starwars.routes.api.v1 import starships as starships_v1
from starwars.routes.api.v1 import vehicles as vehicles_v1
from starwars.services.db import close_db
from starwars.support.os_utils import create_directories_if_needed

LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "std-formatter": {"()": logging.Formatter, "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"}
    },
    "handlers": {"console": {"class": "logging.StreamHandler", "formatter": "std-formatter"}},
    "loggers": {"": {"level": os.getenv("ROOT_LOG_LEVEL", "DEBUG"), "handlers": ["console"]}},
}


def set_project_config(app: Flask):
    """Set project configuration."""
    app.config.from_mapping(DATABASE=os.path.join(app.instance_path, "db.sqlite"), LOGGING=LOGGING_CONFIG)
    app.config.from_pyfile("config.py", silent=True)
    create_directories_if_needed(app.instance_path)
    _set_log_config(app)


def register_routes(app: Flask):
    """Register the app routes, using the Blueprints."""
    app.register_blueprint(healthcheck.blueprint, url_prefix="/api/healthcheck")
    app.register_blueprint(person_v1.blueprint, url_prefix="/api/v1/persons")
    app.register_blueprint(starships_v1.blueprint, url_prefix="/api/v1/starships")
    app.register_blueprint(films_v1.blueprint, url_prefix="/api/v1/films")
    app.register_blueprint(planets_v1.blueprint, url_prefix="/api/v1/planets")
    app.register_blueprint(species_v1.blueprint, url_prefix="/api/v1/species")
    app.register_blueprint(vehicles_v1.blueprint, url_prefix="/api/v1/vehicles")
    app.register_blueprint(pages.blueprint)


def register_custom_commands(app: Flask):
    """Register the app custom cli commands."""
    app.cli.add_command(init_db_command)
    app.cli.add_command(new_user_command)


def set_appcontext_teardown(app: Flask):
    """Set the teardown functions for the app context."""
    app.teardown_appcontext(close_db)


def _set_log_config(app: Flask):
    logging.config.dictConfig(app.config["LOGGING"])
