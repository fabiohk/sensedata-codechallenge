"""Module with custom cli commands for the Flask app."""

import click

from flask.cli import with_appcontext

from starwars.business import create_new_user
from starwars.business import seed_db_with_swapi_content
from starwars.exceptions import UserAlreadyExists
from starwars.services.db import init_db


@click.command("init-db")
@click.option("--seed-swapi", default=False, show_default=True, is_flag=True)
@with_appcontext
def init_db_command(seed_swapi: bool):
    """
    Clear the existing data and create new tables.

    Optionally seed the database with content from SWAPI (--seed-swapi).
    """
    init_db()

    if seed_swapi:
        seed_db_with_swapi_content()
    click.echo("Initialized the database.")


@click.command("new-user")
@click.option("-u", "--username", "username", type=str)
@click.option("-p", "--password", "password", type=str)
@with_appcontext
def new_user_command(username: str, password: str):
    """Create a new user for app usage."""
    try:
        create_new_user(username, password)
    except UserAlreadyExists:
        click.echo(f"User {username} already exists.")
    else:
        click.echo("User succesfully created!")
