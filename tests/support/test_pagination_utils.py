"""Module with tests for the pagination utils helpers methods -- written for pytest."""

import pytest

from starwars.support.pagination_utils import get_page_from_sequence


def test_should_correctly_paginate_a_sequence():
    sequence = [1, 2, 3, 4, 5]

    paginated_sequence = get_page_from_sequence(sequence, page_index=1, page_size=2)
    assert paginated_sequence == {"total": 5, "items": [1, 2]}

    paginated_sequence = get_page_from_sequence(sequence, page_index=2, page_size=2)
    assert paginated_sequence == {"total": 5, "items": [3, 4]}

    paginated_sequence = get_page_from_sequence(sequence, page_index=3, page_size=2)
    assert paginated_sequence == {"total": 5, "items": [5]}

    paginated_sequence = get_page_from_sequence(sequence, page_index=4, page_size=2)
    assert paginated_sequence == {"total": 5, "items": []}


def test_should_raise_value_error_when_page_index_is_not_positive_integer():
    with pytest.raises(ValueError, match="'page_index' must be greater than 0!"):
        get_page_from_sequence([], page_index=0)


def test_should_raise_value_error_when_page_size_is_not_positive_integer():
    with pytest.raises(ValueError, match="'page_size' must be greater than 0"):
        get_page_from_sequence([], page_index=1, page_size=0)
