"""Module with configuration for the pytest."""
import os
import tempfile

from typing import Generator

import pytest

from flask import Flask
from flask.testing import FlaskClient

from starwars.services.db import init_db
from starwars.wsgi import create_app


@pytest.fixture(scope="session", autouse=True)
def app() -> Generator[Flask, None, None]:
    """Start the app for testing purposes."""
    app = create_app()
    app.config["TESTING"] = True

    db_fd, tmp_path = tempfile.mkstemp()
    app.config["DATABASE"] = tmp_path

    with app.app_context():
        init_db()

    yield app

    os.close(db_fd)
    os.unlink(tmp_path)


@pytest.fixture(scope="module")
def client(app: Flask) -> FlaskClient:
    """Start a flask testing client for our app."""
    return app.test_client()
