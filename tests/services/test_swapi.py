"""Module with tests for the SWAPI client -- written for pytest."""

import os

import pytest

from starwars.services.swapi import SWAPIClient

TOTAL_PERSONS = int(os.getenv("TOTAL_PERSON", "82"))
TOTAL_PLANETS = int(os.getenv("TOTAL_PLANETS", "60"))
TOTAL_FILMS = int(os.getenv("TOTAL_FILMS", "6"))
TOTAL_SPECIES = int(os.getenv("TOTAL_SPECIES", "37"))
TOTAL_VEHICLES = int(os.getenv("TOTAL_VEHICLES", "39"))
TOTAL_STARSHIPS = int(os.getenv("TOTAL_STARSHIPS", "36"))


@pytest.fixture
def swapi_client():
    """Create a SWAPIClient instance for testing usage."""
    return SWAPIClient()


def test_should_retrieve_all_persons_from_swapi(swapi_client: SWAPIClient):
    persons = list(swapi_client.get_all_persons())

    assert len(persons) == TOTAL_PERSONS


def test_should_retrieve_all_planets_from_swapi(swapi_client: SWAPIClient):
    planets = list(swapi_client.get_all_planets())

    assert len(planets) == TOTAL_PLANETS


def test_should_retrieve_all_films_from_swapi(swapi_client: SWAPIClient):
    films = list(swapi_client.get_all_films())

    assert len(films) == TOTAL_FILMS


def test_should_retrieve_all_species_from_swapi(swapi_client: SWAPIClient):
    species = list(swapi_client.get_all_species())

    assert len(species) == TOTAL_SPECIES


def test_should_retrieve_all_vehicles_from_swapi(swapi_client: SWAPIClient):
    vehicles = list(swapi_client.get_all_vehicles())

    assert len(vehicles) == TOTAL_VEHICLES


def test_should_retrieve_all_starships_from_swapi(swapi_client: SWAPIClient):
    starships = list(swapi_client.get_all_starships())

    assert len(starships) == TOTAL_STARSHIPS
