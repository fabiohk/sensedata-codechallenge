"""Module with tests for the planet DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import count_table
from tests.utils import new_planet


def test_should_create_a_new_planet(app: Flask):
    with app.app_context():
        total_before = count_table("planet")
        new_planet(app)
        total_after = count_table("planet")

    assert total_after == total_before + 1
