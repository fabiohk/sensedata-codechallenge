"""Module with tests for the film DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import count_table
from tests.utils import new_film


def test_should_create_a_new_film(app: Flask):
    with app.app_context():
        total_before = count_table("film")
        new_film(app)
        total_after = count_table("film")

    assert total_after == total_before + 1
