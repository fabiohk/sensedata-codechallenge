"""Module with tests for the starship DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import count_table
from starwars.services.db.starship import get_starships
from starwars.services.db.starship import get_starships_rated
from tests.utils import new_starship


def test_should_create_a_new_starship(app: Flask):
    with app.app_context():
        total_before = count_table("starship")
        new_starship(app)
        total_after = count_table("starship")

    assert total_after == total_before + 1


def test_should_retrieve_starships_rated_sorted_by_rating(app: Flask):
    new_starship(app, hyperdrive_rating=1.0, cost_in_credits=200)
    new_starship(app, hyperdrive_rating=1.0, cost_in_credits=300)

    with app.app_context():
        starships = get_starships()
        result = get_starships_rated(
            rating_statement="starship.hyperdrive_rating / starship.cost_in_credits",
            sort_order="DESC",
            sort_field="rating",
        )

        assert [
            starship["hyperdrive_rating"] / starship["cost_in_credits"]
            for starship in sorted(
                starships, key=lambda starship: -starship["hyperdrive_rating"] / starship["cost_in_credits"]
            )
        ] == [starship["rating"] for starship in result]
        assert result[0]["rating"] >= result[-1]["rating"]
