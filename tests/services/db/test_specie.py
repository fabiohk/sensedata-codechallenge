"""Module with tests for the specie DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import count_table
from tests.utils import new_specie


def test_should_create_a_new_specie(app: Flask):
    with app.app_context():
        total_before = count_table("specie")
        new_specie(app)
        total_after = count_table("specie")

    assert total_after == total_before + 1
