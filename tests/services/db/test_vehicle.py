"""Module with tests for the vehicle DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import count_table
from tests.utils import new_vehicle


def test_should_create_a_new_vehicle(app: Flask):
    with app.app_context():
        total_before = count_table("vehicle")
        new_vehicle(app)
        total_after = count_table("vehicle")

    assert total_after == total_before + 1
