"""Module with tests for the person DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import DBFilter
from starwars.services.db import FilmCharacter
from starwars.services.db import PersonSpecie
from starwars.services.db import VehiclePilot
from starwars.services.db import count_table
from starwars.services.db.person import get_filtered_persons
from starwars.services.db.person import get_persons
from starwars.services.db.relationship import create_film_character
from starwars.services.db.relationship import create_person_specie
from starwars.services.db.relationship import create_vehicle_pilot
from tests.utils import new_film
from tests.utils import new_person
from tests.utils import new_planet
from tests.utils import new_specie
from tests.utils import new_vehicle


def test_should_create_a_new_person(app: Flask):
    with app.app_context():
        total_before = count_table("person")
        new_person(app)
        total_after = count_table("person")

    assert total_after == total_before + 1


def test_should_filter_persons_by_given_planet(app: Flask):
    planet = new_planet(app)
    persons = [new_person(app, planet) for _ in range(5)]

    with app.app_context():
        filters = [DBFilter(table="person", field="homeworld", value=planet["id"])]
        result = get_filtered_persons(filters=filters)

        assert len(result) == len(persons)
        for person in result:
            assert person["homeworld"] == planet["id"]


def test_should_filter_persons_by_given_film(app: Flask):
    film = new_film(app)
    persons = [new_person(app) for _ in range(5)]

    with app.app_context():
        for person in persons:
            create_film_character(FilmCharacter(character_id=person["id"], film_id=film["id"]))

        filters = [DBFilter(table="film_character", field="film_id", value=film["id"])]
        result = get_filtered_persons(filters=filters)

        assert len(result) == len(persons)
        for person in result:
            assert film["id"] in person["films"]


def test_should_filter_persons_by_given_specie(app: Flask):
    specie = new_specie(app)
    persons = [new_person(app) for _ in range(5)]

    with app.app_context():
        for person in persons:
            create_person_specie(PersonSpecie(person_id=person["id"], specie_id=specie["id"]))

        filters = [DBFilter(table="person_specie", field="specie_id", value=specie["id"])]
        result = get_filtered_persons(filters=filters)

        assert len(result) == len(persons)
        for person in result:
            assert specie["id"] in person["species"]


def test_should_filter_persons_by_given_vehicle(app: Flask):
    vehicle = new_vehicle(app)
    persons = [new_person(app) for _ in range(5)]

    with app.app_context():
        for person in persons:
            create_vehicle_pilot(VehiclePilot(vehicle_id=vehicle["id"], pilot_id=person["id"]))

        filters = [DBFilter(table="vehicle_pilot", field="vehicle_id", value=vehicle["id"])]
        result = get_filtered_persons(filters=filters)

        assert len(result) == len(persons)
        for person in result:
            assert vehicle["id"] in person["vehicles"]


def test_should_filter_persons_by_multiple_filter(app: Flask):
    planet, film, specie, vehicle = new_planet(app), new_film(app), new_specie(app), new_vehicle(app)
    homeworld_filter, film_filter, specie_filter, vehicle_filter = (
        DBFilter(table="person", field="homeworld", value=planet["id"]),
        DBFilter(table="film_character", field="film_id", value=film["id"]),
        DBFilter(table="person_specie", field="specie_id", value=specie["id"]),
        DBFilter(table="vehicle_pilot", field="vehicle_id", value=vehicle["id"]),
    )

    persons = [new_person(app, planet) for _ in range(5)]

    with app.app_context():
        create_film_character(FilmCharacter(film_id=film["id"], character_id=persons[0]["id"]))
        create_person_specie(PersonSpecie(person_id=persons[1]["id"], specie_id=specie["id"]))
        create_vehicle_pilot(VehiclePilot(vehicle_id=vehicle["id"], pilot_id=persons[2]["id"]))

        create_film_character(FilmCharacter(film_id=film["id"], character_id=persons[3]["id"]))
        create_vehicle_pilot(VehiclePilot(vehicle_id=vehicle["id"], pilot_id=persons[3]["id"]))

        result = get_filtered_persons(filters=[homeworld_filter, film_filter])  # Must return persons[0] and persons[3]
        assert len(result) == 2
        for person in result:
            assert film["id"] in person["films"]
            assert planet["id"] == person["homeworld"]
        assert sorted([persons[0]["id"], persons[3]["id"]]) == sorted(person["id"] for person in result)

        result = get_filtered_persons(filters=[homeworld_filter, specie_filter])  # Must return persons[1]
        assert len(result) == 1
        assert specie["id"] in result[0]["species"]
        assert planet["id"] == result[0]["homeworld"]
        assert persons[1]["id"] == result[0]["id"]

        result = get_filtered_persons(
            filters=[homeworld_filter, vehicle_filter]
        )  # Must return persons[2] nad persons[3]
        assert len(result) == 2
        for person in result:
            assert vehicle["id"] in person["vehicles"]
            assert planet["id"] == person["homeworld"]
        assert sorted([persons[2]["id"], persons[3]["id"]]) == sorted(person["id"] for person in result)

        result = get_filtered_persons(filters=[film_filter, vehicle_filter])  # Must return persons[3]
        assert len(result) == 1
        assert vehicle["id"] in result[0]["vehicles"]
        assert film["id"] in result[0]["films"]
        assert persons[3]["id"] == result[0]["id"]

        result = get_filtered_persons(filters=[homeworld_filter, film_filter, vehicle_filter])  # Must return persons[3]
        assert len(result) == 1
        assert vehicle["id"] in result[0]["vehicles"]
        assert film["id"] in result[0]["films"]
        assert planet["id"] == result[0]["homeworld"]
        assert persons[3]["id"] == result[0]["id"]


def test_should_correctly_return_person_sorted_by_name(app: Flask):
    new_person(app, name="Luke Skywalker")
    new_person(app, name="C-3PO")

    with app.app_context():
        persons = get_persons()
        result = get_filtered_persons(sort_field="person.name", sort_order="")
        assert sorted(person["name"] for person in persons) == [person["name"] for person in result]


def test_should_correctly_return_person_sorted_by_gender(app: Flask):
    new_person(app, name="R2-D2", gender="n/a")
    new_person(app, name="Darth Vader", gender="male")

    with app.app_context():
        persons = get_persons()
        result = get_filtered_persons(sort_order="", sort_field="person.gender")

        assert sorted(person["gender"] for person in persons) == [person["gender"] for person in result]


def test_should_correctly_return_person_sorted_by_weight(app: Flask):
    new_person(app, name="Leia Organa", gender="female", weight=49)
    new_person(app, name="Owen Lars", gender="male", weight=120)

    with app.app_context():
        persons = get_persons()
        result = get_filtered_persons(sort_order="", sort_field="person.mass")

        assert sorted(person["mass"] for person in persons) == [person["mass"] for person in result]


def test_should_correctly_return_person_sorted_by_height(app: Flask):
    new_person(app, name="Beru Whitesun lars", gender="female", weight=75, height=165)
    new_person(app, name="R5-D4", gender="n/a", weight=32, height=97)

    with app.app_context():
        persons = get_persons()
        result = get_filtered_persons(sort_order="", sort_field="person.height")

        assert sorted(person["height"] for person in persons) == [person["height"] for person in result]
