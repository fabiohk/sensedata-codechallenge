"""Module with tests for the relationship DB interface -- written for pytest."""

from flask import Flask

from starwars.services.db import FilmCharacter
from starwars.services.db import FilmPlanet
from starwars.services.db import FilmSpecie
from starwars.services.db import FilmStarship
from starwars.services.db import FilmVehicle
from starwars.services.db import PersonSpecie
from starwars.services.db import StarshipPilot
from starwars.services.db import VehiclePilot
from starwars.services.db import count_table
from starwars.services.db.relationship import create_film_character
from starwars.services.db.relationship import create_film_planet
from starwars.services.db.relationship import create_film_specie
from starwars.services.db.relationship import create_film_starship
from starwars.services.db.relationship import create_film_vehicle
from starwars.services.db.relationship import create_person_specie
from starwars.services.db.relationship import create_starship_pilot
from starwars.services.db.relationship import create_vehicle_pilot
from tests.utils import new_film
from tests.utils import new_person
from tests.utils import new_planet
from tests.utils import new_specie
from tests.utils import new_starship
from tests.utils import new_vehicle


def test_should_create_new_film_character(app: Flask):
    film, person = new_film(app), new_person(app)

    with app.app_context():
        total_before = count_table("film_character")

        film_character = FilmCharacter(film_id=film["id"], character_id=person["id"])
        create_film_character(film_character)

        total_after = count_table("film_character")

    assert total_after == total_before + 1


def test_should_create_new_person_specie(app: Flask):
    person, specie = new_person(app), new_specie(app)

    with app.app_context():
        total_before = count_table("person_specie")

        person_specie = PersonSpecie(person_id=person["id"], specie_id=specie["id"])
        create_person_specie(person_specie)

        total_after = count_table("person_specie")

    assert total_after == total_before + 1


def test_should_create_new_vehicle_pilot(app: Flask):
    vehicle, person = new_vehicle(app), new_person(app)

    with app.app_context():
        total_before = count_table("vehicle_pilot")

        vehicle_pilot = VehiclePilot(vehicle_id=vehicle["id"], pilot_id=person["id"])
        create_vehicle_pilot(vehicle_pilot)

        total_after = count_table("vehicle_pilot")

    assert total_after == total_before + 1


def test_should_create_new_film_specie(app: Flask):
    film, specie = new_film(app), new_specie(app)

    with app.app_context():
        total_before = count_table("film_specie")

        film_specie = FilmSpecie(film_id=film["id"], specie_id=specie["id"])
        create_film_specie(film_specie)

        total_after = count_table("film_specie")

    assert total_after == total_before + 1


def test_should_create_new_film_vehicle(app: Flask):
    film, vehicle = new_film(app), new_vehicle(app)

    with app.app_context():
        total_before = count_table("film_vehicle")

        film_vehicle = FilmVehicle(film_id=film["id"], vehicle_id=vehicle["id"])
        create_film_vehicle(film_vehicle)

        total_after = count_table("film_vehicle")

    assert total_after == total_before + 1


def test_should_create_new_film_planet(app: Flask):
    film, planet = new_film(app), new_planet(app)

    with app.app_context():
        total_before = count_table("film_planet")

        film_planet = FilmPlanet(film_id=film["id"], planet_id=planet["id"])
        create_film_planet(film_planet)

        total_after = count_table("film_planet")

    assert total_after == total_before + 1


def test_should_create_new_film_starship(app: Flask):
    film, starship = new_film(app), new_starship(app)

    with app.app_context():
        total_before = count_table("film_starship")

        film_starship = FilmStarship(film_id=film["id"], starship_id=starship["id"])
        create_film_starship(film_starship)

        total_after = count_table("film_starship")

    assert total_after == total_before + 1


def test_should_create_new_starship_pilot(app: Flask):
    starship, person = new_starship(app), new_person(app)

    with app.app_context():
        total_before = count_table("starship_pilot")

        starship_pilot = StarshipPilot(starship_id=starship["id"], pilot_id=person["id"])
        create_starship_pilot(starship_pilot)

        total_after = count_table("starship_pilot")

    assert total_after == total_before + 1
