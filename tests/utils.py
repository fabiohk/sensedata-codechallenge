"""Module with utility methods for tests."""

from datetime import date
from typing import Optional

from flask import Flask

from starwars.services.db import Film
from starwars.services.db import Person
from starwars.services.db import Planet
from starwars.services.db import Specie
from starwars.services.db import Starship
from starwars.services.db import Vehicle
from starwars.services.db.film import create_film
from starwars.services.db.film import get_films
from starwars.services.db.person import create_person
from starwars.services.db.person import get_persons
from starwars.services.db.planet import create_planet
from starwars.services.db.planet import get_planets
from starwars.services.db.specie import create_specie
from starwars.services.db.specie import get_species
from starwars.services.db.starship import create_starship
from starwars.services.db.starship import get_starships
from starwars.services.db.vehicle import create_vehicle
from starwars.services.db.vehicle import get_vehicles


def new_planet(app: Flask) -> Planet:
    """Create a new planet and return its created information."""
    with app.app_context():
        planet = Planet(
            name="Tatooine",
            rotation_period="23",
            orbital_period="304",
            diameter="10465",
            climate="arid",
            gravity="1 standard",
            terrain="desert",
            surface_water="1",
            population="200000",
        )
        create_planet(planet)
        planets = get_planets(limit=1)
    return planets[0]


def new_person(
    app: Flask,
    planet: Optional[Planet] = None,
    name: Optional[str] = None,
    gender: Optional[str] = None,
    weight: Optional[float] = None,
    height: Optional[float] = None,
) -> Person:
    """Create a new person and return its created information."""
    planet = new_planet(app) if not planet else planet

    with app.app_context():
        person = Person(
            name="Luke Skywalker" if not name else name,
            birth_year="19BBY",
            eye_color="blue",
            gender="male" if not gender else gender,
            hair_color="blond",
            height=172 if not height else height,
            mass=77 if not weight else weight,
            skin_color="fair",
            homeworld=planet["id"],
        )
        create_person(person)
        persons = get_persons()
    return persons[0]


def new_film(app: Flask) -> Film:
    """Create a new film and return its created information."""
    with app.app_context():
        film = Film(
            title="A New Hope",
            episode_id=4,
            opening_crawl="It is a period of civil war.",
            director="George Lucas",
            producer="Gary Kurtz, Rick McCallum",
            release_date=date(year=1977, month=5, day=25),
        )
        create_film(film)
        films = get_films()
    return films[0]


def new_starship(
    app: Flask, cost_in_credits: Optional[float] = None, hyperdrive_rating: Optional[float] = None
) -> Starship:
    """Create a new starship and return its created information."""
    with app.app_context():
        starship = Starship(
            name="CR90 corvette",
            model="CR90 corvette",
            manufacturer="Corellian Engineering Corporation",
            cost_in_credits=3_500_000 if not cost_in_credits else cost_in_credits,
            length="150",
            max_atmosphere_speed="950",
            crew="30065",
            passengers="600",
            cargo_capacity="3000000",
            consumables="1 year",
            hyperdrive_rating=2.0 if not hyperdrive_rating else hyperdrive_rating,
            MGLT="60",
            starship_class="corvette",
        )
        create_starship(starship)
        starships = get_starships()
    return starships[0]


def new_vehicle(app: Flask) -> Vehicle:
    """Create a new vehicle and return its created information."""
    with app.app_context():
        vehicle = Vehicle(
            name="Sand Crawler",
            model="Digger Crawler",
            manufacturer="Corellia Mining Corporation",
            cost_in_credits="150000",
            length="36.8",
            max_atmosphere_speed="30",
            crew="46",
            passengers="30",
            cargo_capacity="50000",
            consumables="2 months",
            vehicle_class="wheeled",
        )
        create_vehicle(vehicle)
        vehicles = get_vehicles()
    return vehicles[0]


def new_specie(app: Flask) -> Specie:
    """Create a new specie and return its created information."""
    planet = new_planet(app)

    with app.app_context():
        specie = Specie(
            name="Human",
            classification="mammal",
            designation="sentient",
            average_height="180",
            skin_colors="caucasian, black, asian, hispanic",
            hair_colors="blonde, brown, black, red",
            eye_colors="brown, blue, green, hazel, grey, amber",
            average_lifespan="120",
            language="Galactic Basic",
            homeworld=planet["id"],
        )
        create_specie(specie)
        species = get_species()
    return species[0]
