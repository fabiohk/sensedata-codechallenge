"""Module with tests for the healthcheck routes -- written for pytest."""

from flask.testing import FlaskClient


def test_should_return_200_when_all_services_are_up(client: FlaskClient):
    response = client.get("/api/healthcheck/")

    assert response.status_code == 200
