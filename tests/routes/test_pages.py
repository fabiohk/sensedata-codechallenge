"""Module with tests for the app pages -- written for pytest."""

from flask import Flask
from flask.testing import FlaskClient
from parsel import Selector

from starwars.business import retrieve_starships_rated
from tests.utils import new_starship


def test_should_correctly_render_starship_page(app: Flask, client: FlaskClient):
    new_starship(app, hyperdrive_rating=2.0, cost_in_credits=1000)
    new_starship(app, hyperdrive_rating=1.0, cost_in_credits=1000)

    page = client.get("/starships/")
    with app.app_context():
        rated_starships = retrieve_starships_rated()

    selector = Selector(text=str(page.data))
    starship_table = selector.xpath("//table[@id='starship']/tbody/tr").getall()
    for i, row in enumerate(starship_table):
        assert str(rated_starships[i]["id"]) in row
        assert rated_starships[i]["name"] in row
        assert rated_starships[i]["model"] in row
        assert str(rated_starships[i]["rating"]) in row
